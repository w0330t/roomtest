package net.blueness.roomtestforkotlin

import android.arch.persistence.room.*

@Dao
interface UserDao{
    /**
     * 查询
     * @return
     */
    @Query("SELECT * FROM user_table")
    abstract fun getAllUsers(): List<User>

    /**
     * 添加
     * @param users
     */
    @Insert
    abstract fun insertUser(vararg users: User)

    /**
     * 更新
     * @param users
     */
    @Update
    abstract fun updateUser(vararg users: User)

    /**
     * 删除
     * @param users
     */
    @Delete
    abstract fun deleteUser(vararg users: User)
}