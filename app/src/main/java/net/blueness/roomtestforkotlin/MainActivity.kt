package net.blueness.roomtestforkotlin

import android.annotation.SuppressLint
import android.arch.persistence.room.Room
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers

class MainActivity : AppCompatActivity() {

    private val TAG = "输出"

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        Thread(Runnable { databaseOperation() }).start()


        Observable
                .create(ObservableOnSubscribe<List<User>> { emitter ->
                    Log.d(TAG, "Observable thread is : ${Thread.currentThread().name}")
                    Log.d(TAG, "emit 1")
                    emitter.onNext(databaseOperation())
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Consumer<List<User>> {
                    override fun accept(t: List<User>?) {
                        Log.d(TAG, "Observer thread is : ${Thread.currentThread().name}")
                        Log.d(TAG, "onNext: ${t.toString()}")
                    }
                })



        setContentView(R.layout.activity_main)

    }


    private fun databaseOperation(): List<User> {
        val mUserDatabase = Room.databaseBuilder(applicationContext, UserDatabase::class.java, "users").build()
        val mUserDao = mUserDatabase.getUserDao()

        //写数据库
        Log.d(TAG, "开始写入数据...")
        writeDatabase(mUserDao, "张三", 18)
        writeDatabase(mUserDao, "李四", 19)
        Log.d(TAG, "写入数据库完毕.")

        //读数据库
        Log.d(TAG, "第1次读数据库")
        return readDatabase(mUserDao)

    }

    private fun readDatabase(dao: UserDao): List<User> {
        Log.d(TAG, "读数据库...")
        val users = dao.getAllUsers()
        for (u in users) {
            Log.d(TAG, u.id.toString() + "," + u.name + "," + u.age + "," + u.updateTime)
        }
        Log.d(TAG, "读数据库完毕.")
        return users
    }


    private fun writeDatabase(dao: UserDao, name: String, age: Int) {
        val user = User()
        user.name = name
        user.age = age
        user.updateTime = System.currentTimeMillis()
        dao.insertUser(user)
    }
}
