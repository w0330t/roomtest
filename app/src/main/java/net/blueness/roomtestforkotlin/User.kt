package net.blueness.roomtestforkotlin

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey


//每个@Entity类表示一个表中的实体。 如果表与名称不同，可以指定表名。
@Entity(tableName = "user_table")
data class User(
        // 主键，自加一
        // data 类必须加一个主要构造参数
        @PrimaryKey(autoGenerate = true)
        var id: Int = 0
){

    // 列名
    @ColumnInfo(name = "userName")
    var name: String = ""

    @ColumnInfo(name = "userAge")
    var age: Int = 0

    @ColumnInfo(name = "updateTime")
    var updateTime: Long = 0
}
